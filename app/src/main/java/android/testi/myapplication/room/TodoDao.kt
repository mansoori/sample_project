package ir.namaa.payapp.data.utils.database

import android.testi.myapplication.room.LocationResponse
import androidx.room.*

@Dao
interface TodoDao {


    // MOSTRAR DATOS Y ORDENARLOS
    @Query("SELECT * from todoTable  ORDER BY id ASC")
    fun getAllWords(): MutableList<LocationResponse>

    // AÑADIR DATOS
    @Insert
    fun insert(word: LocationResponse)

    // ELIMINAR ALL DATA
    @Query("DELETE FROM todoTable")
    fun deleteAll()

    // ACTUALIZAR DATOS
    //Sin Query
    @Update
    fun update(word: LocationResponse)

    //Con Query
    @Query("UPDATE todoTable SET lat = :word WHERE id == :id")
    fun updateItem(word: String, id: Int)

    // BORRAR ITEM
    @Delete
    fun deleteWord(word: LocationResponse)

}

