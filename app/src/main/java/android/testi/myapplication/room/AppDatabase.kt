package ir.namaa.payapp.data.utils.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context
import android.testi.myapplication.App
import android.testi.myapplication.room.LocationResponse

@Database(entities = [LocationResponse::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun appDao(): TodoDao

    companion object {

        fun buildDatabase(context: Context): AppDatabase = Room.databaseBuilder(
            App.context!!,
            AppDatabase::class.java,
            "DataBase_Test"
        ).build()

    }

}

