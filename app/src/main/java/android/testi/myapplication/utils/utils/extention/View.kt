package ir.fram.work.util.extention

import android.content.Context
import android.view.View
import android.widget.Toast


fun View.toGone() {
    visibility = View.GONE
}

fun View.toVisibile() {
    visibility = View.VISIBLE
}

fun View.toInVisibile() {
    visibility = View.INVISIBLE
}

fun toast(message: String, context: Context) {
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}
