package android.testi.myapplication.di.module

import android.testi.myapplication.ui.MainActivity
import android.testi.myapplication.ui.MainViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ir.fram.work.di.ViewModelKey


@Module(includes = [MainActivityModule.ProvideViewModel::class])
class MainActivityModule {


    @Module
    class InjectViewModel {
        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: MainActivity
        ) = ViewModelProviders.of(target, factory).get(MainViewModel::class.java)

    }

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(MainViewModel::class)
        fun provideViewModel(
        ): ViewModel =
            MainViewModel()

    }
}
