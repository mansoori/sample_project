package ir.fram.work.di.module

import android.app.Application
import android.content.Context
import android.testi.myapplication.data.repository.AppRepository
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ir.fram.work.di.BaseViewModelFactory
import ir.namaa.payapp.data.utils.database.AppDatabase
import ir.namaa.payapp.data.utils.database.TodoDao
import javax.inject.Provider
import javax.inject.Singleton


@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application.applicationContext



    @Provides
    @Singleton
    fun provideDatabase(context: Context) = AppDatabase.buildDatabase(context)

    @Provides
    fun provideTodoDao(context: Context) = AppDatabase.buildDatabase(context).appDao()

    @Provides
    fun provideViewModelFactory(
        providers: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
    ): ViewModelProvider.Factory =
        BaseViewModelFactory(providers)


    @Provides
    @Singleton
    fun provideRepository(
        context: Context,
        dao: TodoDao
    ) = AppRepository(context, dao)

}
