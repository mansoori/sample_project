package ir.fram.work.di.builder


import android.testi.myapplication.ui.MainActivity
import android.testi.myapplication.di.module.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector
import ir.fram.work.di.scope.PerActivity


@Module
abstract class MainActivityBuilder {

    @PerActivity
    @ContributesAndroidInjector(modules = [MainActivityModule::class, MainActivityModule.InjectViewModel::class])
    abstract fun bindMainActivity(): MainActivity
}
