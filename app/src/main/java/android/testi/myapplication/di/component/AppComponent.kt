package android.testi.myapplication.di.component

import android.app.Application
import android.testi.myapplication.App
import android.testi.myapplication.ui.MainViewModel
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import ir.fram.work.di.builder.MainActivityBuilder
import ir.fram.work.di.module.AppModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        (AndroidInjectionModule::class),
        (AppModule::class),
        (MainActivityBuilder::class)]
)

interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(instance: Application)
    //
    fun inject(baseViewModel: MainViewModel)

}

