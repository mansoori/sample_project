package android.testi.myapplication.data.repository

import android.content.Context
import android.os.AsyncTask
import android.testi.myapplication.room.LocationResponse
import androidx.lifecycle.MutableLiveData
import ir.namaa.payapp.data.utils.database.TodoDao
import javax.inject.Inject

class AppRepository @Inject constructor(
   var context: Context,
   var wordDao: TodoDao
) {
//////////////////////////////////////////////////////////////////////////


    //        @WorkerThread
    fun insert(word: LocationResponse, liveData: MutableLiveData<Boolean>) {
//        wordDao.insert(word)
        addWordAsyncTask(wordDao, word, liveData).execute()
    }


    /* --------------- BORRAR TODOS LOS DATOS -------------- */

    fun deleteAll() {
        deleteAllWordsAsyncTask(wordDao).execute()
    }

    private class deleteAllWordsAsyncTask internal constructor(private val mAsyncTaskDao: TodoDao) :
        AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {
            mAsyncTaskDao.deleteAll()
            return null
        }
    }

    /* ---------------- BORRAR UN SOLO DATO ---------------- */

    fun deleteWord(word: LocationResponse) {
        deleteWordAsyncTask(wordDao).execute(word)
    }

    private class deleteWordAsyncTask internal constructor(private val mAsyncTaskDao: TodoDao) :
        AsyncTask<LocationResponse, Void, Void>() {

        override fun doInBackground(vararg params: LocationResponse): Void? {
            mAsyncTaskDao.deleteWord(params[0])
            return null
        }
    }
/////////////////////////////////////////////////////////////////////

    private class addWordAsyncTask internal constructor(
        private val mAsyncTaskDao: TodoDao,
        var word: LocationResponse,
        var liveData: MutableLiveData<Boolean>
    ) :
        AsyncTask<LocationResponse, Void, Void>() {

        override fun doInBackground(vararg params: LocationResponse): Void? {
            mAsyncTaskDao.insert(word)
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            liveData.value = true
        }
    }

    //////////////////////////////////////////////////////////////////////

    fun getDataCash(liveData: MutableLiveData<MutableList<LocationResponse>>) {
        getWordAsyncTask(wordDao, liveData).execute()
    }
    private class getWordAsyncTask internal constructor(
        private val dao: TodoDao,
        var liveData: MutableLiveData<MutableList<LocationResponse>>
    ) :
        AsyncTask<LocationResponse, Void, MutableList<LocationResponse>>() {

        override fun doInBackground(vararg params: LocationResponse): MutableList<LocationResponse>? {
            return dao.getAllWords()

        }

        override fun onPostExecute(result: MutableList<LocationResponse>?) {
            super.onPostExecute(result)

            liveData.value = result
        }

    }


//////////////////////////////////////////////////////////////////////


/* -------------- ACTUALIZAR UN SOLO DATO ---------------- */

    fun update(word: LocationResponse) {
        updateWordAsyncTask(wordDao).execute(word)
    }

    private class updateWordAsyncTask internal constructor(private val mAsyncTaskDao: TodoDao) :
        AsyncTask<LocationResponse, Void, Void>() {
        override fun doInBackground(vararg params: LocationResponse?): Void? {
            mAsyncTaskDao.update(params[0]!!)
            return null
        }
    }




}
