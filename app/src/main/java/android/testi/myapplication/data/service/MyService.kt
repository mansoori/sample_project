package android.testi.myapplication.data.service

import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.location.Location
import android.os.*
import android.testi.myapplication.room.LocationResponse
import android.text.TextUtils
import android.widget.Toast
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.location.*
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import ir.namaa.payapp.data.utils.database.AppDatabase
import ir.namaa.payapp.data.utils.database.TodoDao
import java.util.*


class MyService : Service() {

    private var timer = Timer()
    private var updateProfile: TimerTask = CustomTimerTask(this)
    private lateinit var mFusedLocationClient: FusedLocationProviderClient

    private var dao = AppDatabase.buildDatabase(this).appDao()
    private var mFirebaseDatabase: DatabaseReference? = null
    private var mFirebaseInstance: FirebaseDatabase? = null
    private var locationId: String? = null

    override fun onCreate() {

        super.onCreate()


        Toast.makeText(this, "Service Started.Please Wait", Toast.LENGTH_SHORT).show()
        timer.scheduleAtFixedRate(updateProfile, 0, 10000)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)


        mFirebaseInstance = FirebaseDatabase.getInstance()

        mFirebaseDatabase = mFirebaseInstance!!.getReference("locationsInfo")

        mFirebaseInstance!!.getReference("test_app").setValue("Realtime Database For Locations")

    }


    override fun onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy()
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_SHORT).show()
        timer.cancel()
    }


    override fun onBind(intent: Intent): IBinder? {
        return null
    }


    private class CustomTimerTask(var service: MyService) : TimerTask() {
        private val mHandler = Handler()


        override fun run() {
            Thread(Runnable {
                mHandler.post {
                    service.getLastLocation()

                }
            }).start()

        }

    }

    @SuppressLint("MissingPermission")
    fun getLastLocation() {

        mFusedLocationClient.lastLocation.addOnCompleteListener { task ->
            val location: Location? = task.result
            if (location == null) {
                requestNewLocationData()
            } else {

                registerDataToDb(location)
            }
        }

    }

    private fun registerDataToDb(location: Location) {
        val todoResponse = LocationResponse(location.latitude.toString(), location.longitude.toString(), 0)
        insertTdo(todoResponse)

        val intent = Intent("ReceiverAction")
        val bundle = Bundle()
        bundle.putString("latitude", location.latitude.toString())
        bundle.putString("longitude", location.longitude.toString())
        intent.putExtras(bundle)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)

    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation
            registerDataToDb(mLastLocation)
        }
    }


    private fun insertTdo(word: LocationResponse) {
        WordAsyncTask(dao, word, locationId, mFirebaseDatabase).execute()
    }


    private class WordAsyncTask internal constructor(
        private val mAsyncTaskDao: TodoDao,
        var word: LocationResponse,
        var userId: String?,
        var mFirebaseDatabase: DatabaseReference?

    ) :
        AsyncTask<LocationResponse, Void, Void>() {

        override fun doInBackground(vararg params: LocationResponse): Void? {
            mAsyncTaskDao.insert(word)
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

            if (TextUtils.isEmpty(userId)) {
                userId = mFirebaseDatabase?.push()?.key
            }

            userId?.let {
                mFirebaseDatabase?.child(it)?.setValue(word)

            }

        }
    }

}

