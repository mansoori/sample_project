package ir.namaa.app.shared.listeners

interface PermisionListener {
    fun onGranted()
    fun onDenied()
}