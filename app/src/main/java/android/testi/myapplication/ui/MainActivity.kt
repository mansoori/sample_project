package android.testi.myapplication.ui

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.testi.myapplication.R
import android.testi.myapplication.data.service.MyService
import android.testi.myapplication.room.LocationResponse
import android.testi.myapplication.ui.adapter.LocationsAdapter
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import dagger.android.support.DaggerAppCompatActivity
import ir.fram.work.util.extention.toast
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : DaggerAppCompatActivity() {


    @Inject
    lateinit var mainViewModel: MainViewModel

    private var myReceiver: MyBroadcastReceiver? = null

    private var adapter: LocationsAdapter? = null

    override fun onResume() {
        super.onResume()
        myReceiver = MyBroadcastReceiver()
        val intentFilter = IntentFilter("ReceiverAction")
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver!!, intentFilter)

    }

    override fun onPause() {
        super.onPause()
        if (myReceiver != null)
            LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver!!)
        myReceiver = null

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel.also {
            lifecycle.addObserver(it)
        }
        mainViewModel.getDataCash()
        mainViewModel.allWordss.observe(this, Observer {
            adapter = LocationsAdapter(this, it)
            recycler_view.adapter = adapter
        })
        Dexter.withActivity(this)
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse) {
                    if (isLocationEnabled()) {
                        val intentMyService = Intent(this@MainActivity, MyService::class.java)
                        ContextCompat.startForegroundService(this@MainActivity, intentMyService)
                    } else {
                        toast(getString(R.string.turn_on_gps), this@MainActivity)
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 10)
                    }

                }

                override fun onPermissionDenied(response: PermissionDeniedResponse) {
                    finish()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).check()


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === 10 && resultCode === 0) {
            val provider = Settings.Secure.getString(contentResolver, Settings.Secure.LOCATION_PROVIDERS_ALLOWED)
            if (provider != null && provider.isNotEmpty()) {
                val intentMyService = Intent(this@MainActivity, MyService::class.java)
                startService(intentMyService)
                toast(getString(R.string.please_wait_min), this)
            } else {
                toast(getString(R.string.turn_on_gps), this)
            }
        }
    }

    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager


        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }



    inner class MyBroadcastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            // Here you have the received broadcast
            // And if you added extras to the intent get them here too
            // this needs some null checks
            val b = intent.extras
            val lat = b!!.getString("latitude")
            val lon = b!!.getString("longitude")
            adapter?.let {
                val todo = LocationResponse(lat!!, lon!!, 0)
                adapter!!.addlist(todo)

            }
        }
    }
}
