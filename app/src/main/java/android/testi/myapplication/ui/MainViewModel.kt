package android.testi.myapplication.ui

import android.testi.myapplication.App
import android.testi.myapplication.data.repository.AppRepository
import android.testi.myapplication.room.LocationResponse
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class MainViewModel :  ViewModel(), LifecycleObserver {

    private var parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    var allWordss = MutableLiveData<MutableList<LocationResponse>>()
    var isAdd = MutableLiveData<Boolean>()
    @Inject
    lateinit var appRepository: AppRepository


//    abstract fun init()

    init {
        App.appComponent.inject(this)
    }


    fun insertTdo(word: LocationResponse) = scope.launch(Dispatchers.IO) {
        appRepository.insert(word, isAdd)
    }

    fun getDataCash() {
        appRepository.getDataCash(allWordss)
    }

}
