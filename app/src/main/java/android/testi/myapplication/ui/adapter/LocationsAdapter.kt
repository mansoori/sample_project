package android.testi.myapplication.ui.adapter

import android.content.Context
import android.testi.myapplication.R
import android.testi.myapplication.room.LocationResponse
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.adapter_locations.view.*


class LocationsAdapter(private val mContext: Context, list: MutableList<LocationResponse>) :
    RecyclerView.Adapter<LocationsAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(itemList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.adapter_locations, parent, false)
        return ViewHolder(v)
    }

    var itemList: MutableList<LocationResponse> = list


    override fun getItemCount(): Int {
        return itemList.size
    }

    fun addlist(list: LocationResponse) {
        itemList.add(list)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(items: LocationResponse) {


            itemView.tv_lat.text = items.lat
            itemView.tv_lon.text = items.lon

        }

    }
}

