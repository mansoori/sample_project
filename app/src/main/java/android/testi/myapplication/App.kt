package android.testi.myapplication

import android.annotation.SuppressLint
import android.content.Context
import android.testi.myapplication.di.component.AppComponent
import android.testi.myapplication.di.component.DaggerAppComponent
import androidx.appcompat.app.AppCompatDelegate
import com.google.firebase.database.FirebaseDatabase
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication


class App : dagger.android.support.DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        appComponent = DaggerAppComponent.builder().application(this).build()
        return appComponent
    }


    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        FirebaseDatabase.getInstance().setPersistenceEnabled(true)

    }


    companion object {
        lateinit var appComponent: AppComponent
        @SuppressLint("StaticFieldLeak")
        internal var context: Context? = null

    }


}
